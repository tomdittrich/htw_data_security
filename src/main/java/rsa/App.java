package rsa;

import java.math.BigInteger;

/**
 * Description
 *
 * @author Tom Dittrich s0555944@htw-berlin.de
 * @version 0.1
 */
public class App {
    public static void main(String[] args) {
//        KeyGenerator keyGenerator = new KeyGenerator();

//        keyGenerator.generateNumbers();
//        keyGenerator.printNumbers();

        BigInteger nKey = new BigInteger("");
        BigInteger eKey = new BigInteger("");
        BigInteger dKey = new BigInteger("");

        BigInteger messageToEncrypt = new BigInteger("");
        System.out.println("Message to encrypt: " + messageToEncrypt);

        BigInteger messageToDecrypt = new BigInteger("");
        System.out.println("Message to decrypt: " + messageToDecrypt);

        BigInteger encryptedMessage = EncryptMessage.encrypt(nKey, eKey, messageToEncrypt);
        System.out.println("Encrypted: " + encryptedMessage);

        BigInteger decryptedMessage = DecryptMessage.deencrypt(nKey, dKey, messageToDecrypt);
        System.out.println("Decrypt: " + decryptedMessage);
    }
}
